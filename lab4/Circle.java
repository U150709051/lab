public class Circle
{
	int radius;

	public double area()
	{
		double cirarea = 3.14 * (Math.pow(radius, 2));
		return cirarea;
	}
	public double parimetre()
	{
		double cirprim = 2 * 3.14 * radius;
		return cirprim;
	}
}
