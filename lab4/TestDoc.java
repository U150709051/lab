public class TestDoc
{
	public static void rectan()
	{
		Rectangle rectangle = new Rectangle();
		rectangle.sideA = 5;
		rectangle.sideB = 6;
		int rectangleArea = rectangle.area();
		int rectanglePari = rectangle.parimetre();

		System.out.println("The Area of rectangle with side of 5cm and 6cm: " + rectangleArea);
		System.out.println("The parametre of the same rectangle: " + rectanglePari);
	}
	public static void circ()	
	{
		Circle circle = new Circle();
		circle.radius = 10;
		double circleArea = circle.area();
		double circlePari = circle.parimetre();

		System.out.println("This is the Area of a circle with radius of 10cm: " + circleArea);
		System.out.println("This is the parametre of the same circle: " + circlePari);
	}
	
	public static void main(String[] test)
	{
		rectan();
		circ();
	}
}
