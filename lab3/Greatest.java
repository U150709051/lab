public class Greatest
{
	public static void main(String[] args)
	{
		int no1 = Integer.parseInt(args[0]);
		int no2 = Integer.parseInt(args[1]);
		int no3 = Integer.parseInt(args[2]);
		int no4 = Integer.parseInt(args[3]);
		int no5 = Integer.parseInt(args[4]);
		
		boolean res1 = no1 > no2;
		int comp1 = res1 ? no1 : no2;
		
		boolean res2 = no3 > no4;
		int comp2 = res2 ? no3 : no4;
		
		boolean res3 = comp1 > comp2;
		int comp3 = res3 ? comp1 : comp2;

		boolean res4 = comp3 > no5;
		int comp4 = res4 ? comp3 : no5;

		System.out.println("This is the Greates number: " + comp4);
	}
}
