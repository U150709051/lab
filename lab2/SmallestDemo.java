public class SmallestDemo
{
	public static void main(String[] args)
	{

		int value1 = Integer.parseInt(args[0]);
		int value2 = Integer.parseInt(args[1]);
		int value3 = Integer.parseInt(args[2]);
		int result;
	
		boolean condition = value1 < value2;

		result = condition ? value1 : value2;
		
		condition = result < value3;
			
		int subresult = condition ? result : value3;
		
		System.out.println(subresult);
	
	}
}
		
