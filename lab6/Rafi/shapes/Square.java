package Rafi.shapes;
public class Square
{
	double side;
	
	public Square(double s)
	{
		side = s;
	}
	
	public double area()
	{
		double res = Math.pow(side,2);
		return res;
	}
}
