package Rafi.main;
import Rafi.shapes.*;
import java.util.ArrayList;

public class Main

{
	public static void main(String[] main)
	{
		Circle circle1 = new Circle(8);
		Circle circle2 = new Circle(9);
		Circle circle3 = new Circle(10);
		
		System.out.println("The circle areas from the package: Rafi/shapes/Circle.java");		
		System.out.println(circle1.area());
		System.out.println(circle2.area());
		System.out.println(circle3.area());	
		System.out.println();

		Square square = new Square(4);
		System.out.println("The square area from the package: Rafi/shapes/Square.java");
		System.out.println(square.area());
		System.out.println();
		
		ArrayList<Circle> circ = new ArrayList<Circle>();
		circ.add(new Circle(8));
		circ.add(new Circle(9));
		circ.add(new Circle(10));
		
		System.out.println("The circle area from the package: ArrayList");
		for (int r=0; r<circ.size(); r++)
		{
			System.out.println(circ.get(r).area());
		} 
	}
}
