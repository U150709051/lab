package Rafi.Shapes3D;

public class Circle
{
	int radius;
	
	public Circle(int r)
	{
		radius = r;
	}
	
	public double area()
	{
		return 3.14 * Math.pow(radius,2);
	}
}
