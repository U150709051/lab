package Rafi.Shapes3D;
import Rafi.Shapes3D.Circle;

public class Cylinder extends Circle
{
	private int height;
	
	public Cylinder(int radius, int h)
	{
		super(radius);
		height = h;
	}
	
	public double area()
	{
		return (2 * 3.14 * radius * height) + ( 2 * super.area());
	}
	
	public double volume()
	{
		return 3.14 * Math.pow(radius,2) * height;
	}
	
	public String toString()
	{
		return "radius:" + radius + " heigth:" + height;
	}
}
