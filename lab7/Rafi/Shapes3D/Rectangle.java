package Rafi.Shapes3D;

public class Rectangle
{
	protected double width;
	protected double length;
	
	public Rectangle(double w, double l)
	{
		width = w;
		length = l;
	}
	
	public double area()
	{
		return width * length;
	}
	
	public double parametre()
	{
		return 2 * (width + length);
	}
}
