package Rafi.Main;
import Rafi.Shapes3D.Box;
import Rafi.Shapes3D.Cylinder;

public class Test3D
{
	public static void main(String[] main)
	{
		Cylinder cylinder = new Cylinder(5,6);
		
		System.out.println("These are the details of the Cylinder:");
		System.out.println();
		System.out.println("toString details: " + cylinder.toString());
		System.out.println("Area of the Cylinder: " + cylinder.area());
		System.out.println("Volume of the Cylinder: " + cylinder.volume());
		System.out.println(); 
		
		Box box = new Box(4,5,6);
		
		System.out.println("These are the details of the 3D Box:");
		System.out.println();
		System.out.println("toString details: " + box.toString());
		System.out.println("Area of the Box " + box.area());
		System.out.println("Volume of the Box " + box.volume());
		System.out.println();
		
	
	}
}
